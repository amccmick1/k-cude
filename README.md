# K-Cude

Kinect NUI for control of Compiz Cube in Ubuntu. Currently installs Prototype 1 with one command.     

To use, simply clone this repo or download and:    
Open a terminal    
Change to the directory where you downloaded the repo    
Change directory to libs/install    
Install with the following command:         
```sudo ./install.sh```    
Run with    
```cd /usr/local/bin```    
```sudo ./skeltrack-desktop-control```    
    
For maximum effect set up Compiz Config so that 'rotate cube initiate' is bound to control + mouse button 3 (right-click). Then you can move the mouse with your right hand and to rotate the cube hold up your left hand whilst moving your right hand in the direction you wish to rotate the cube. Instructions on how to configure Compiz can be found online until this README is updated.    
    
---------------------------------------------------------------------------------------------------------------
    
## Prototype 1

#### Update v0.0.3 - 10/04/2015

Installs:    

* Libfreenect Driver for Kinect (Xbox 360)    
* Gfreenect Wrapper    
* Skeltrack Library    
* Modified Skeltrack Desktop Control Application
* CompizConfig, Compiz Cube and Compiz Rotate Cube plugins

#### Update v0.0.2 - 23/02/2015

Installs:    

* Libfreenect Driver for Kinect (Xbox 360)    
* Gfreenect Wrapper    
* Skeltrack Library    
* Skeltrack Desktop Control    

#### Update v0.0.1 - 06/02/2015

Installs:    

* Libfreenect Driver for Kinect (Xbox 360)    
* Gfreenect Wrapper    
* Skeltrack Library     

## Prototype 2

#### Update v0.0.1 - 06/02/2015

Installs:    

* Libfreenect2 Driver for Kinect v2 (Xbox One)    

## Issues

* Wifi Hardware is blocked on newer laptops shipped with Windows 8.1. To bypass this we download open souce drivers for the Realtek wifi hardware and switch on the hardware in the rfkill userspace    
* Shared libraries for Skeltrack and Skeltrack Desktop Control must have their links updated with ```sudo ldconfig {executable_dir}```
* To compile Skeltrack Desktop Control the -lm flag had to be added to the configure file to link math library otherwise it fails with sqrt@@... function failure. This is fixed with command ```./configure LIBS="-lm"```    
* To compile Skeltrack library enable-tests in the configuration has been switched off    
* Libfreenect driver works on older versions of the Linux Kernel but the Libfreenect2 driver required updating to the latest kernel as of 06/02/2015    
* Libfreenect2 driver RGB stream works but Depth and IR feeds only work when using CPU and not GPU Depth Packet Processing

## Unblock Realtek Wifi (included)

To unlock the blocked Wifi hardware:    

* Open a terminal    
* Change to the directory where you downloaded the repo    
* Change directory to libs/install    
* Install realtek wifi drivers with:    
```sudo ./unlockwifi.sh```

The process should enable automatic unlocking of the Wifi on startup of the OS but if this does not work then the user can unlock the wifi manually after install on each startup:    

* Open a terminal    
* Change directory to /etc   
* Run Wifi unblock script with:
```sudo ./rc.local```

## Testing

Test System 1 Specification:    
* ubuntu 14.04 LTS    
* Memory - 15.6GiB    
* Processor - Intel Core i7-4820k-CPI @ 3.70GHz x 8    
* Graphics - Gallium 0.4 on NVE6    
* OS type 64-bit    
* Disk 101.2GB    

*Note: Disk is a SSD and Gallium 0.4 on NVE6 refers to the Ubuntu default X.org Nouveau display driver running on an NVIDIA GTX 650TI graphics card*    

Test System 2 Specification:    
* ubuntu 14.04 LTS    
* Memory - 2.9GiB    
* Processor - Intel Core 2 Duo CPU P8600 @ 2.40GHz x 2   
* Graphics - Gallium 0.4 on NV98    
* OS type 64-bit    
* Disk 242.9GB    

*Note: Disk is a HDD and Gallium 0.4 on NV98 refers to the Ubuntu default X.org Nouveau display driver running on on-board intel graphics on a Toshiba Tecra laptop*    

Test System 3 Specification:    
* ubuntu 14.04 LTS    
* Memory - 3.7GiB    
* Processor - Intel Celeron(R) CPU N2840 @ 2.16GHz × 2     
* Graphics - Intel Bay Trail    
* OS type 64-bit    
* Disk 488.0GB    

*Note: Disk is a HDD and Intel Bay Trail is the default on-board Intel graphics driver running on a Lenovo G50-30 laptop*