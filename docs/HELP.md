#K-Cude in Lots of Words...    
    
##Why    
    
There are a lot of words in K-Cude I don't understand... Well this is the right place for you. Provided my attempted explanations are followable. I will explain how the program works in detail backwards from the main process.     
    
##Words    
    
###```int main (int argc, char *argv[ ])  {```     

#####Calls
Called by: Auto-execution on program start-up    
Calls: ```on_new_kinect_device ()```   	

#####Returns
```0``` on success or ```-1``` on failure

#####Variables
int ```argc``` -> ```argc``` stands for argument count. This should be the total number of arguments you wish to pass into your main program.

int ```argv[]``` -> ```argv[]``` stands for argument vector. In practice this is an array of characters whose array size should be equal to the ```argc``` integer.

#####Xlib
```XOpenDisplay```, ```XDefaultScreenOfDisplay```, ```XWidthOfScreen``` and ```XHeightOfScreen``` are all methods in Xlib. The X Window System handles user input and accepts output requests from other programs. Xlib is a C language X interface allowing interaction with the X Window System.    

* Requires: ```include <X11/Xlib.h>```
* External Interaction: X Window System (commonly known as X)

#####Clutter
Clutter is an open-source OpenGL GObject-based graphics library. In main we call clutter_init with the arguments passed by the main method (```argc``` and ```argv[]```) to check that clutter exists and initializes correctly. If it fails then we close the X display, return -1 and exit the program.  

* Requires: ```include <clutter/clutter.h>```
* External Interaction: Clutter    

#####Gfreenect
Gfreenect is a wrapper for the Kinect written in GNOME library Glib. It allows the Kinect to be called as a GObject. By calling ```gfreenect_device_new (0, GFREENECT_SUBDEVICE_CAMERA, NULL, on_new_kinect_device, NULL)``` we are creating a new GObject of the Kinect where ```0``` is the first Kinect plugged into the pc, ```GFREENECT_SUBDEVICE_CAMERA``` is the camera driver of the Kinect (includes IR and RGB), ```NULL``` refers to the asynchronous call being non-cancellable, ```on_new_kinect_device``` being the callback method called when the device is instanced and the second ```NULL``` referring to passed user data to the method. The device is instanced with ```gfreenect_device_new_finish ()``` which is called on a successful call of ```gfreenect_device_new ()```.

* Requires: ```include <gfreenect.h>```
* External Interaction: Glib

#####Glib

Glib provides the core object system used in GNOME along with the main loop implementation and utility functions. This allows us to create and use GObjects.

* Requires: ```include <glib-object.h>```
* External Interaction: GNOME  

#####Summary

* ```main ()``` (by default) doesn't require any arguments    
* X is used to create a new display and save in the display variable for use with other methods    
* The ```*``` before the screen variable turns the variable into a pointer referencing the memory address of the variables value.    
* ```XDefaultScreenOfDisplay``` is a screen pointer which is used in this case to fetch width and height of the screen    
* ```argc``` and ```argv[]``` from the ```main ()``` method are passed to clutter_init with ```(argc, &argv)```    
* ```signal (SIGINT, quit)``` actually tells the program to quit. This is needed due the program being asynchronous with no exit condition. SIGINT refers to Signal Interrupt (usually the equivalent to the user pressing Ctrl+C in the terminal while a process is running), and the quit is the quit() method called when CTRL+C is pressed.    
* Since the ```signal ()``` method quits the program the following code is only executed if the program either encounters an error and has to stop or is closed by the user. The signal method is called only after a loop called through ```on_new_kinect_device``` is stopped.    
* ```clutter_main ()``` starts the clutter main loop.    
* ```g_slice_free (type, mem)``` is a glib call which frees a block of memory. Here it is used to clear the blocks of memory storing the ```last_left_point``` and the ```last_right_point``` as a fall-back if the program was unable to clear it earlier in its process tree   
* ```g_object_unref (gpointer object)``` is called to decrease the reference count of an object. If an object's reference count reaches 0 then the object's memory is freed    
* ```XCloseDisplay (display)``` is then called to close the connection to the X server   
* The ```main ()``` method then returns a 0 exit status and closes 

###```}```    

###```on_new_kinect_device (GObject *obj, GAsyncResult *res, gpointer user_data) {```

#####Calls
Called by: ```main ()```     
Calls: ```set_info_text ()```, ```create_instructions ()```, ```on_depth_frame ()```, ```on_texture_draw ()```, ```on_destroy ()```, ```on_key_release ()```    

#####Variables

gobject ```*obj``` -> GObject gfreenect device (Kinect) passed from the function ```gfreenect_device_new``` from which this function is a callback.    

GAsyncResult  ```*res``` -> Holds results of the attempted asynchronous operation from the function ```gfreenect_device_new``` from which this function is a callback.       

gpointer ```user_data``` -> User_data returned from the function ```gfreenect_device_new``` from which this function is a callback.        

#####Skeltrack

Skeltrack is a device agnostic library for the tracking of human skeleton motion based on mathematical computations. In this case it is implemented with a Gfreenect wrapper for use with the Kinect.

* Requires: ```include <skeltrack.h>```
* External Interaction: Skeltack  

#####Summary

* The Kinect is instantiated as a Gfreenect device and assigned to the variable "kinect" with ```gfreenect_device_new_finish()``` called by the callback from ```gfreenect_device_new()``` from the ```main()``` method    
 * Failure:
 * If this fails then g_debug is called to output to terminal that the device could not be created along with the ```error->message``` associated    
 * The error is then freed from glib with ```g_error_free(error) ```   
 * The Clutter main loop is then exited with ```clutter_main_quit ()```    
* ```g_debug()``` is used by Glib to output messages to the terminal
* A Clutter stage is created and assigned to variable stage for use in other methods. The stage created is the default stage as it is a singleton instance and will always be the same stage no matter where is is called from    
* A Clutter stage is a top level window which you can place child actors on. It is also itself an actor
* The Clutter stage (the program's window) has the title "Skeltrack Desktop Control" set by ```clutter_stage_set_title``` and its dimensions set by ```clutter_actor_set_size```
 * Notice the width and height of ```clutter_actor_set_size``` are equal to the native rgb stream resolution of the Kinect at 640, 480
 * The height is set by "height + 220" so there is room for 640 x 220 pixels worth of written instructions for the program itself underneath the ```depth_tex```     
* ```clutter_stage_set_user_resizable``` allows the user to resize the window of the program    
* The aforementioned ```depth_tex``` is added to Clutter as an actor with the Kinect resolution 640 x 480 with ```clutter_container_add_actor (CLUTTER_CONTAINER (stage), depth_tex)```    
 * Note: ```clutter_container_add_actor``` is used to add ClutterActor to a container. This function is now depreciated    
* Another Clutter actor is created with ```clutter_text_new()``` which is populated with text by the ```set_info_text ()``` method    
* The Clutter text actor ```info_text``` is then positioned with ```clutter_actor_set_position ()```. In English this means that the text has a margin left of 50 pixels and a top margin of 20 pixels, positioned under the ```depth_tex``` actor    
* Another clutter actor is created, this time by the ```create_instructions ()``` method    
* It is then positioned again with a 50 pixel left-margin and a 90 pixel top margin after the ```depth_tex``` actor giving the ```info_text``` actor 90-20 pixels of space (70 pixel height)    
 * Note: The reason ```set_info_text``` and ```create_instructions``` are separate methods is because the text in ```set_info_text``` is changed through key press events so it needs to be re-called whereas ```create_instructions``` is constant and does not need calling more than once.
 * This is also why ```info_text``` has to be defined as a top-level ClutterActor variable and not a local one    
* All actors (```depth_tex```, ```info_text``` and ```instructions```) are added to the stage with ```clutter_actor_show_all (stage)```   
* GCallback functions are then connected to a signal for the following objects: ```stage```    
  * This is done with: ```g_signal_connect(instance, detailed_signal, c_handler, data)```
   * In practice this means that when the ```stage``` is destroyed (window closed) Clutter updates its signal to "destroy" which then runs the function ```on_destroy ()```    
   * And when a key is released from the keyboard Clutter updates its signal to "key-release-event" which runs the ```on_key_release ()``` function    
*  A new Skeltrack skeleton is created with ```skeltrack_skeleton_new ()``` and stored in variable ```SkeltrackSkeleton skeleton```    
* ```g_object_get ()``` is called to get the "smoothing-factor" property of the GObject skeleton
 * Note: A GObject can have multiple properties set. In this case we are getting a pre-existing property which has been created in the Skeltrack library to smooth the tracking of the Skeleton based on Holt's double exponential processing on the points returned by Skeltrack    
* GCallback functions are then connected to a signal for the following objects: ```kinect``` and ```depth_tex```
 * This is done with: ```g_signal_connect(instance, detailed_signal, c_handler, data)```
 * In practice this means that whenever the Kinect receives a new depth image the signal "depth-frame" is updated in Gfreenect which then tracks the skeleton again through the callback ```on_depth_frame ()```    
 * When ```on_depth_frame ()``` is called it calls the ```skeltrack_skeleton_track_joints ()``` method which runs the ```on_track_joints ()``` method as a callback after it has found the Skeltrack joint list
 * ```on_track_joints ()``` then takes the Skeltrack joint list and saves it in the variable ```SkeltrackJointList list``` [used later in the ```paint_joint ()``` method]
 * If ```SHOW_SKELETON``` is true then the Clutter Cairo texture is invalidated with ```clutter_cairo_texture_invalidate ()``` and the ```depth_tex``` object sends back a "draw" signal which triggers a callback of ```on_texture_draw``` from the ```g_signal_connect``` in the ```on_new_kinect_device``` method. ```on_texture_draw``` draws the tracked skeleton into the ```ClutterActor depth_tex``` by clearing the texture, painting it white and then drawing the joints on with the ```paint_joint ()``` method    
 * If ```SHOW_SKELETON``` is false then the program returns from ```on_track_joints ()``` to the ```on_depth_frame ()``` where it checks that ```SHOW_SKELETON``` is false and calls the ```create_grayscale_buffer ()``` method. This returns a ```guchar grayscale_buffer``` which is then used to update the ```CLUTTER_TEXTURE (depth_tex)``` with the ```clutter_texture_set_from_rgb_data ()``` method effectively producing an efficient low resolution point cloud called for every depth frame   
* The Kinect's tilt angle is then set to the lowest the motorized pivot can lower itself to with ```gfreenect_device_set_tilt_angle (kinect, 0, NULL, NULL, NULL)``` where 0 is the angle    
* The Kinect's depth stream is started with ```gfreenect_device_start_depth_stream (kinect, GFREENECT_DEPTH_FORMAT_MM, NULL)```     
 * Note: When this method is called; the signal "depth-frame" is triggered whenever a new frame is available    
 * To stop the stream ```gfreenect_device_stop_depth_stream ()``` is called. This is called in the ```on_destroy ()``` method of K-Cude    
 
###```}```    

###```set_info_text (void) {```

#####Calls
Called by: ```on_new_kinect_device ()```     

#####Summary

*  Populates the ```ClutterActor info_text``` with text showing user-selected option variable values which can be updated    
 * If ```SHOW_SKELETON``` is true then build title text "Skeleton", if it's false build title text "Point Cloud"
 * Add current threshold to title text
 * If ```ENABLE_SMOOTHING``` is true then build title text "Yes", if it's false then build title text "No"
 * Add current smoothing factor to title text
* Show title text with ```clutter_text_set_markup (CLUTTER_TEXT (info_text), title)```
* Since ```*title``` is a pointer, free the memory at this address with ```g_free (title)``` after updating ```ClutterActor info_text```    
 
###```}```    

###```create_instructions (void) {```

#####Calls
Called by: ```on_new_kinect_device ()```      

#####Returns
```ClutterActor *create_instructions```       

#####Summary

* Populates the ```ClutterActor instructions``` in ```on_new_kinect_device``` with text one time (not ever updated after run-time)    
    
###```}```    

###```on_destroy (ClutterActor *actor, gpointer data)```     

#####Calls
Called by: ```on_new_kinect_device ()```   

#####Variables

ClutterActor ```*actor``` -> ClutterActor ```*actor``` is passed the ```stage``` ClutterActor through the ```G_CALLBACK (on_destroy)``` method when the destroy signal is sent by ```stage```

gpointer ```data``` -> untyped pointer    

#####Summary

* Gets the Gfreenect device using the gpointer data
* Stops the Kinect depth-stream with the Gfreenect ```gfreenect_device_stop_depth_stream (device, NULL)``` method   
* Exits Clutter main loop with ```clutter_main_quit ()```    
    
###```}```    

###```on_key_release (ClutterActor *actor, ClutterEvent *event, gpointer data)```    

#####Calls
Calls: ```set_info_text ()```   
Called by: ```on_new_kinect_device ()```   

#####Returns
```FALSE``` if ```event = NULL``` otherwise returns ```TRUE``` 

#####Variables

ClutterActor ```*actor``` -> ClutterActor ```*actor``` is passed the ```stage``` ClutterActor through the ```G_CALLBACK (on_key_release)``` method when the key-release-event signal is sent by ```stage```    

ClutterEvent ```*event``` -> ClutterEvent ```*event``` is passed the ```stage``` ClutterEvent through the ```G_CALLBACK (on_key_release)``` method when the key-release-event signal is sent by ```stage```    

gpointer ```data``` -> untyped pointer    

#####Summary

* Return ```FALSE``` if there is no ClutterEvent ```event``` data. Else:    
* Get the Gfreenect device using the gpointer data    
* Get the guint ```key``` from ```clutter_event_get_key_symbol (event)```    
* Run a switch statement on the guint ```key``` to determine the button pressed    
 * Case: Space -> Update ```SHOW_SKELETON``` to opposite of ```SHOW_SKELETON``` so true to false, false to true which will show either ```Skeleton``` or ```Point Cloud``` (true to false)    
 * Case: + -> Add ```100``` to threshold variable    
 * Case: - -> Add ```-100``` to threshold variable    
 * Case: Up_arrow -> Add ```5``` degrees to tilt angle    
 * Case: Down_arrow -> Add ```-5``` degrees to tilt angle    
 * Case: s -> Update ```ENABLE_SMOOTHING``` to opposite of ```ENABLE_SMOOTHING``` so true to false, false to true which will show either ```Yes``` or ```No``` (true to false)    
 * Case: Right_arrow -> Add ```0.05``` to smoothing factor    
 * Case: Left_arrow -> Add ```-0.05``` to smoothing factor    
* Update ```ClutterActor *info_text``` with new ```set_info_text ()``` method call    
* Return ```TRUE```   
    
###```}```    

###```on_depth_frame (GFreenectDevice *kinect, gpointer user_data)```    

#####Calls
Calls: ```on_track_joints ()```, ```process_buffer ()```, ```create_grayscale_buffer ()```   
Called by: ```on_new_kinect_device ()```   

#####Variables

GFreenectDevice  ```*kinect``` -> GFreenectDevice ```*kinect``` is a top level pointer for the Kinect device. It is not passed to this method through the ```g_signal_connect``` GCALLBACK but assigned through ```kinect = gfreenect_device_new_finish (res, &error)``` in the ```on_new_kinect_device``` method

gpointer ```data``` -> untyped pointer     

#####Summary

* ```gboolean smoothing_enabled``` is a left-over variable from testing and isn't actually used   
* ```gfreenect_device_get_depth_frame_raw (kinect, &len, &frame_mode)``` fetches a single raw depth frame from the Kinect    
 * Where ```&len``` is a pointer to retrieve the length of the returned frame data    
 * And ```&frame_mode``` is a GFreenectFrameMode structure to fill the attributes of the frame    
 * Note: Using ```&len``` as the example; ```&``` is a way to reference the ```len``` local variable    
* Then K-Cude takes the width and height from the returned ```frame_mode``` along with the returned depth data, the returned ```&dimension_factor``` (default is 16) and the threshold limits and buffers the data    
* Once the data has been buffered the buffered data is sent to the Skeltrack ```sketrack_skeleton_track_joints ()``` method    
* ```on_track_joints ()``` is the callback assigned to the Skeltrack method. Once Skeltrack has finished tracking the joints in that depth frame ```on_track_joints ()``` is called    
    
###```}```    

###```on_track_joints (GObject *obj, GAysncResult *res, gpointer user_data)```    

#####Calls
Calls: ```interpret_gestures ()```, ```on_texture_draw ()```    
Called by: ```on_depth_frame ()```   

#####Variables

GObject  ```*obj``` -> Skeltrack Skeleton GObject    

GAsyncResult ```*res``` -> success or failure of the GAsync call    

gpointer ```user_data``` -> ```buffer_info`` from Skeltrack     

#####Summary

* The ```buffer_info``` is passed from ```skeltrack_skeleton_track_joints ()``` run in the ```on_depth_frame ()``` method into the ```on_track_joints ()``` callback method through the ```gpointer user_data``` variable    
* ```buffer_info``` is used to populate the neccessary variables in this method in preperation for calling the ```interpret_gestures ()``` method into which it passes the returned skeltrack joint list, the original buffer data and the height and width of the buffer    
* ```on_track_joints ()``` then calls the ```interpret_gestures ()``` method which provides the top level functionality for K-Cude    
 * At this point, if ```SHOW_SKELETON``` is ```TRUE``` then the ```depth_tex``` ClutterActor is invalidated and therefore a new skeleton is drawn through the ```depth_tex``` clutter signal ```draw``` being invoked    
 * Else if ```SHOW_SKELETON``` is ```FALSE``` then the program goes back to the ```on_depth_frame ()``` method    
* Now with ```SHOW_SKELETON = FALSE``` K-Cude sends the data to be buffered as grayscale and the Clutter ```depth_tex``` is updated with this new texture    
* ```g_slice_free1 ()``` is then called to free the memory block holding the grayscale buffer data    
    
###```}```    

###```interpret_gestures (SkeltrackJointList joint_list, guint16 *buffer, guint width, guint height)```    

#####Calls
Calls: ```smooth_point ()```, ```set_mouse_pointer ()```, ```mouse_down ()```    
Called by: ```on_track_joints ()```   

#####Variables

SkeltrackJointList  ```joint_list``` -> Skeltrack Skeleton GObject    

guint16 ```*buffer``` -> buffer    

guint ```width``` ->  buffer width    

guint ```height``` ->  buffer height     

#####Summary

* Interprets gestures by first defining if there is one or two hands present in the frame    
 * If ```single_point``` is true then ```set_mouse_pointer ()``` is called to control mouse movement by taking the x + y position of the hand    
 * Else if there is a ```left_point``` and a ```right_point``` present then it triggers the right-click button of the mouse to be pressed and held down with ```mouse_down(3)```
    
###```}```
