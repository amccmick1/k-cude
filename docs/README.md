# K-Cude

Kinect NUI for control of Compiz Cube in Ubuntu.

##Variables
--------------------------------------------------------------------

static SkeltrackSkeleton *skeleton = NULL;    
static GFreenectDevice *kinect = NULL;    
static ClutterActor *info_text;    
static ClutterActor *depth_tex;    
static SkeltrackJointList list = NULL;    
static gboolean SHOW_SKELETON = TRUE;    
static gboolean ENABLE_SMOOTHING = TRUE;    
static gfloat SMOOTHING_FACTOR = .0;    
static Display *display = NULL;    
static gint screen_width = 0;    
static gint screen_height = 0;    
static gint GESTURE_THRESHOLD = 250;    
static guint GESTURE_TIMEOUT = 1000;    
static guint THRESHOLD_BEGIN = 500;    
static guint THRESHOLD_END   = 1500;    
static ClutterEventType pointer_1_event_type = CLUTTER_NOTHING;    
static ClutterEventType pointer_2_event_type = CLUTTER_NOTHING;    
typedef struct _Point Point;    
static Point *pointer_1 = NULL;    
static Point *pointer_2 = NULL;    
static Point *last_left_point = NULL;    
static Point *last_right_point = NULL;    
static gint64 pointer_enter_time = 0;    
static gint old_distance = -1;    
static guint last_key = 0;    

typedef struct
{
  guint16 *reduced_buffer;
  guint16 *original_buffer;
  gint width;
  gint height;
  gint reduced_width;
  gint reduced_height;
} BufferInfo;    
struct _Point
{
  gint x;
  gint y;
  gint z;
};    
static Point *
smooth_point (guint16 *buffer,
              guint width,
              guint height,
              SkeltrackJoint *joint);

##Functions
--------------------------------------------------------------------
    
* get_distance (Point *point_a, Point *point_b)
* get_pointer_position (Display *display, gint *x, gint *y)
* set_mouse_pointer (gint x, gint y)
* key_down (guint keycode)
* key_up (guint keycode)
* mouse_down (guint button)
* mouse_up (guint button)
* mouse_click (guint button)
* hand_is_active (SkeltrackJoint *head, SkeltrackJoint *hand)
* both_hands_left (void)
* interpret_guestures (SkeltrackJointList joint_list,
                     guint16 *buffer,
                     guint width,
                     guint height)
* on_track_joints (GObject      *obj,
                 GAsyncResult *res,
                 gpointer      user_data)
* grayscale_buffer_set_value (guchar *buffer, gint index, guchar value)
* process_buffer (guint16 *buffer,
                guint width,
                guint height,
                guint dimension_factor,
                guint threshold_begin,
                guint threshold_end)
* create_grayscale_buffer (BufferInfo *buffer_info, gint dimension_reduction)
* smooth_point (guint16 *buffer, guint width, guint height, SkeltrackJoint *joint)
* on_depth_frame (GFreenectDevice *kinect, gpointer user_data)
* paint_joint (cairo_t *cairo,
             SkeltrackJoint *joint,
             gint radius,
             const gchar *color_str)
* on_texture_draw (ClutterCairoTexture *texture,
                 cairo_t *cairo,
                 gpointer user_data)
* set_info_text (void)
* set_threshold (gint difference)
* set_tilt_angle (GFreenectDevice *kinect, gdouble difference)
* enable_smoothing (gboolean enable)
* set_smoothing_factor (gfloat factor)
* on_key_release (ClutterActor *actor,
                ClutterEvent *event,
                gpointer data)
* create_instructions (void)
* on_destroy (ClutterActor *actor, gpointer data)
* on_new_kinect_device (GObject      *obj,
                      GAsyncResult *res,
                      gpointer      user_data)
* quit (gint signale)
* main (int argc, char *argv[])
