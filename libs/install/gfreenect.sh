# Create and/or Navigate to Build Directory
cd ..
mkdir -p build
cd build

# Install Gfreenect Wrapper
PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig/:/usr/local/lib/pkgconfig:$
export PKG_CONFIG_PATH
rm -rf Gfreenect
git clone https://github.com/elima/GFreenect.git Gfreenect
cd Gfreenect
git clean -f
libtoolize -v --copy --install
aclocal
autoconf
sudo ./autogen.sh
make && sudo make install
sudo ldconfig /usr/local/lib/
cd ..

