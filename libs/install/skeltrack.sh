# Create and/or Navigate to Build Directory
cd ..
mkdir -p build
cd build

# Install Skeltrack Library
rm -rf Skeltrack
git clone https://github.com/joaquimrocha/Skeltrack.git Skeltrack
cd Skeltrack
./autogen.sh
./configure --enable-tests=no --enable-examples=yes
make && sudo make install
cd ..

