# Create and/or Navigate to Build Directory
cd ..
mkdir -p build
cd build

# Install Libfreenect2 Drivers
rm -rf Libfreenect2
git clone https://github.com/OpenKinect/libfreenect2.git Libfreenect2
sudo apt-get install build-essential libturbojpeg libtool autoconf li$
cd Libfreenect2/depends
sh install_ubuntu.sh
sudo ln -s /usr/lib/x86_64-linux-gnu/libturbojpeg.so.0.0.0 /usr/lib/x$
cd ../examples/protonect/
sudo rm -f Protonect.cpp
sudo cp ../../../../src/Libfreenect2_Protonect_src_file_cpupacketpipe$
cmake CMakeLists.txt
sudo make && sudo make install
#sudo ./bin/Protonect
cd ../../..

