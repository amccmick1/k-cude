# Create and/or Navigate to Build Directory
cd ..
mkdir -p build
cd build

# Install Skeltrack-Desktop-Control
git clone https://github.com/joaquimrocha/Skeltrack-Desktop-Control.git Skeltrack-Desktop-Control
cd Skeltrack-Desktop-Control
sudo ./autogen.sh
sudo ./configure LIBS="-lm"
cd src
cp ../../../src/Skeltrack-Desktop-Control_mainCfile/main.c .
cd ..
sudo make && sudo make install
sudo ldconfig /usr/local/bin/
cd ..
