# Install Dependencies
# Dependencies for Compiling : automake build-essential cmake cmake-c$
# Dependency Libraries : lib*
# Dependency Tools : gitcore openjdk-6-jdk python unzip
# Dependencies for Documentation : doxygen gtk-doc-tools
sudo apt-get install autoconf automake build-essential cmake cmake-curses-gui \
compizconfig-settings-manager compiz-plugins compiz-plugins-extra doxygen freeglut3-dev \
g++ git-core graphviz gobject-introspection gtk-doc-tools \
libboost-all-dev libeigen3-dev libclutter-1.0-dev libflann-dev libgirepository1.0-dev \
libglew-dev libgtest-dev libgtk2.0-dev libpcap-dev libqhull-dev libsuitesparse-dev libtool \
libusb-1.0-0-dev libvtk5-dev libvtk5-qt4-dev libxmu-dev libxi-dev libxtst-dev \
python unzip \
--force-yes -y
