# Create and/or Navigate to Build Directory
cd ..
mkdir -p build
cd build

# Install Libfreenect Drivers
rm -rf Libfreenect
git clone https://github.com/OpenKinect/libfreenect.git Libfreenect
cd Libfreenect
mkdir build
cd build
cmake -L ..
make && sudo make install
cd ../..
